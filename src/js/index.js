document.addEventListener("DOMContentLoaded", function(event) {
    // FullPage
    new fullpage('#fullpage', {
        //options here
        autoScrolling:true,
        scrollHorizontally: true,
        scrollOverflow:true,

        onLeave: function(origin, destination, direction) {
            var buttonUp = document.getElementById('goToTop');
            if (destination.index !== 0) {
                
                buttonUp.classList.add('show');
            } else {
                buttonUp.classList.remove('show');
            }
        }
    });



    (function() {
        var throttle = function(type, name, obj) {
            obj = obj || window;
            var running = false;
            var func = function() {
                if (running) { return; }
                running = true;
                 requestAnimationFrame(function() {
                    obj.dispatchEvent(new CustomEvent(name));
                    running = false;
                });
            };
            obj.addEventListener(type, func);
        };

        /* init - you can init any event */
        throttle("resize", "optimizedResize");
    })();

    // handle event
    // window.addEventListener("optimizedResize", function() {
    //     const header = document.querySelector('header');
    //     const wraper = document.querySelector('header .wrap');
    //     if (window.innerWidth > 500 && window.innerWidth < 766) {            
    //         wraper.setAttribute("style", `height:${header.clientHeight}px;`);
    //     } else {
    //         wraper.setAttribute("style", `height:auto;`);
    //     }
    // });

    
    //ninja fix
    let menuItemsX = document.querySelectorAll('.menuItemOuter');
    menuItemsX = Array.from(menuItemsX);
    $('.menuItemOuter').hide();

    var video = document.getElementById('myVideo');

    video.addEventListener("ended", function () {
        $('.menuItemOuter').show();
        setTimeout(function(){
            menuItemsX.forEach(function (item) {
                item.classList.add(('played'))
            })
        }, 7000);
    });



    //ninja fix
    if (window.innerWidth > 767) {
        var svg = document.querySelector('header svg');
        svg.setAttribute("style", `display:none;`);
        var video = document.getElementById('myVideo');
        video.onended = function() {
            svg.setAttribute("style", `display:block;`);
        }
    }

    //Only on tablets and desctop
    if (window.outerWidth > 767) {

        let menuItems = document.querySelectorAll('.menuItemOuter');
        menuItems = Array.from(menuItems);

        menuItems.forEach(function(item){
            item.addEventListener('mouseover', function(e){
                const nameOfMenuItem = this.getAttribute("data-name");
                const svgPath = document.querySelector(`svg .${nameOfMenuItem}`);
                svgPath.classList.add('show');
            })
            item.addEventListener('mouseout', function(e){
                const nameOfMenuItem = this.getAttribute("data-name");
                const svgPath = document.querySelector(`svg .${nameOfMenuItem}`);
                svgPath.classList.remove('show');
            })
        })
    }
    

    // function go_top() {
    //         var btn = document.querySelector('#goToTop');
    //         let body = document.querySelector('html');
    //         console.log(body);

    //         window.onscroll = function() {
    //             var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    //             if (scrolled > 200) {
    //                 btn.classList.add('show');
    //             } else {
    //                 btn.classList.remove('show');
    //             }
    //         }

    //         btn.addEventListener('click', function(e){
    //             e.preventDefault();

    //             scrollToTop(1000);
    //         });

    //         function scrollToTop(scrollDuration) {
    //             var cosParameter = window.scrollY / 2,
    //                 scrollCount = 0,
    //                 oldTimestamp = performance.now();
    //             function step (newTimestamp) {
    //                 scrollCount += Math.PI / (scrollDuration / (newTimestamp - oldTimestamp));
    //                 if (scrollCount >= Math.PI) window.scrollTo(0, 0);
    //                 if (window.scrollY === 0) return;
    //                 window.scrollTo(0, Math.round(cosParameter + cosParameter * Math.cos(scrollCount)));
    //                 oldTimestamp = newTimestamp;
    //                 window.requestAnimationFrame(step);
    //             }
    //             window.requestAnimationFrame(step);
    //         }


    //             /* 
    //                 Explanations:
    //                 - pi is the length/end point of the cosinus intervall (see above)
    //                 - newTimestamp indicates the current time when callbacks queued by requestAnimationFrame begin to fire.
    //                   (for more information see https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame)
    //                 - newTimestamp - oldTimestamp equals the duration

    //                   a * cos (bx + c) + d                      | c translates along the x axis = 0
    //                 = a * cos (bx) + d                          | d translates along the y axis = 1 -> only positive y values
    //                 = a * cos (bx) + 1                          | a stretches along the y axis = cosParameter = window.scrollY / 2
    //                 = cosParameter + cosParameter * (cos bx)    | b stretches along the x axis = scrollCount = Math.PI / (scrollDuration / (newTimestamp - oldTimestamp))
    //                 = cosParameter + cosParameter * (cos scrollCount * x)
    //             */           

    //     } go_top();

        // Vanilla JavaScript Scroll to Anchor

        // function scrollTo() {
        //     const links = document.querySelectorAll('.scroll');
        //     links.forEach(each => (each.onclick = scrollAnchors));
        // }

        // scrollTo();



        // function scrollAnchors(e, respond = null) {
        //     const distanceToTop = el => Math.floor(el.getBoundingClientRect().top);
        //     e.preventDefault();
        //     var targetID = (respond) ? respond.getAttribute('href') : this.getAttribute('href');
        //     const targetAnchor = document.querySelector(targetID);
        //     if (!targetAnchor) return;
        //     const originalTop = distanceToTop(targetAnchor);
        //     window.scrollBy({ top: originalTop, left: 0, behavior: 'smooth' });
        //     const checkIfDone = setInterval(function() {
        //         const atBottom = window.innerHeight + window.pageYOffset >= document.body.offsetHeight - 2;
        //         if (distanceToTop(targetAnchor) === 0 || atBottom) {
        //             targetAnchor.tabIndex = '-1';
        //             targetAnchor.focus();
        //             window.history.pushState('', '', targetID);
        //             clearInterval(checkIfDone);
        //         }
        //     }, 100);
        // }

});


$(function(){



    // container is the DOM element;

    let counter = 0;
    const container = $(".slogan p");

    const arr = [
        ["increase ", "Profits with ", "Sustainable ", "Programs"],
        ["integrated ", "Platform to ", "Stop ", "Pollution"],
        ["innovative ", "Partner to ", "Stop ", "Pollution"],
        ["interdisciplinary ", "Provider of ", "Solutions and ", "Platforms"],
        ["interlink ", "People for ", "Sustainable ", "Projects"],
        ["is ", "Profit the ", "Societies ", "Problem"],
        ["interrupt ", "Pollution to ", "Save the ", "Planet"],
        ["innovative ", "Projects will ", "Safe the ", "Planet"],
        ["independent ", "Provider for ", "Systems and ", "Platforms"],
        ["imagine ", "People will ", "Stop the ", "Plastic"],
        ["innovative ", "Provider for ", "Smart ", "Systems"],
        ["improve ", "People’s ", "Skills for our ", "Planet"],
        ["innovative ", "Provider for ", "Sustainable ", "Payment"],
        ["involve ", "Politics to ", "Save the ", "Planet"]
    ];

    // const arr = [
    //     "increase Profits with Sustainable Programs",
    //     "integrated Platform to Stop Pollution",
    //     "innovative Partner to Stop Pollution",
    //     "interdisciplinary Provider of Solutions and Platforms",
    //     "interlink People for Sustainable Projects",
    //     "is Profit the Societies Problem",
    //     "interrupt Pollution to Save the Planet",
    //     "innovative Projects will Safe the Planet",
    //     "independent Provider for Systems and Platforms",
    //     "imagine People will Stop the Plastic",
    //     "innovative Provider for Smart Systems",
    //     "improve People’s Skills for our Planet",
    //     "innovative Provider for Sustainable Payment",
    //     "involve Politics to Save the Planet"
    // ];  

    // Shuffle the contents of container
    container.shuffleLetters();

    // Change slogan every 5 seconds

    setInterval(function() {
        if (counter > arr.length - 1) { 
            counter = 0; 
        }
        container.each(function(index){
            $(this).shuffleLetters({text: arr[counter][index]});
        })

        counter++;
    },5000);

    /*document.getElementById('myVideo').addEventListener('ended', myHandler, false);

    function myHandler(e) {
        var videoFile = 'worms_loop.mp4';
        $(' #myVideo source').attr('src', videoFile);
        $('#myVideo').attr("loop", true);
        $("#myVideo")[0].load();
        document.getElementById('myVideo').removeEventListener('ended', myHandler, false)
    }*/

    $(window).resize(function(){
        if (window.innerWidth > 767) {
            var svg = document.querySelector('header svg');
            //svg.setAttribute("style", `display:none;`);
            var video = document.getElementById('myVideo');
            video.onended = function() {
                svg.setAttribute("style", `display:block;`);

            }
        }

        //Only on tablets and desctop
        if (window.outerWidth > 767) {

            let menuItems = document.querySelectorAll('.menuItemOuter');
            menuItems = Array.from(menuItems);

            menuItems.forEach(function(item){
                item.addEventListener('mouseover', function(e){
                    const nameOfMenuItem = this.getAttribute("data-name");
                    const svgPath = document.querySelector(`svg .${nameOfMenuItem}`);
                    svgPath.classList.add('show');
                })
                item.addEventListener('mouseout', function(e){
                    const nameOfMenuItem = this.getAttribute("data-name");
                    const svgPath = document.querySelector(`svg .${nameOfMenuItem}`);
                    svgPath.classList.remove('show');
                })
            })
        }
    })
    
});